require "spec_helper"
require "dashboard"

describe Dashboard do
  describe "#posts" do
    it "returns posts visible to the current user" do
      posts = double("posts")
      user = double("user")
      posts_visible = double("posts_visible")
      allow(posts).to receive(:visible_to).with(user).and_return(posts_visible)

      result = Dashboard.new(posts: posts, user: user).posts

      expect(result).to eq(posts_visible)
    end
  end
end
